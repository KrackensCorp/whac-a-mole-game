package ru.andreyshiryaew.whacamole.data

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class DataModel : ViewModel() {
    val score: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }

    val record: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }

    var recordValue: Int = 0

}