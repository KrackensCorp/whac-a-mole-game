package ru.andreyshiryaew.whacamole

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import ru.andreyshiryaew.whacamole.data.DataModel
import ru.andreyshiryaew.whacamole.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private val dataModel: DataModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        dataModel.score.observe(this) {

        }
        dataModel.record.observe(this) {

        }
    }
}