package ru.andreyshiryaew.whacamole.endgame

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.findNavController
import ru.andreyshiryaew.whacamole.R
import ru.andreyshiryaew.whacamole.data.DataModel
import ru.andreyshiryaew.whacamole.databinding.FragmentEndgameBinding


class EndgameFragment : Fragment(R.layout.fragment_endgame) {

    private lateinit var binding: FragmentEndgameBinding
    private val dataModel: DataModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEndgameBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataModel.score.observe(activity as LifecycleOwner) {
            binding.scoreTextView.text = it.toString()
        }

        // session record
        binding.recordTextView.text = dataModel.recordValue.toString()

        binding.startGameButton.setOnClickListener { findNavController().navigate(R.id.action_endgameFragment_to_gameFragment) }
        binding.menuButton.setOnClickListener { findNavController().navigate(R.id.action_endgameFragment_to_menuFragment) }
    }

}