package ru.andreyshiryaew.whacamole.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import ru.andreyshiryaew.whacamole.R
import ru.andreyshiryaew.whacamole.data.DataModel
import ru.andreyshiryaew.whacamole.databinding.FragmentMenuBinding


class MenuFragment : Fragment(R.layout.fragment_menu) {

    private lateinit var binding: FragmentMenuBinding
    private val dataModel: DataModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMenuBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // session record
        binding.textView2.text = "Record value: " + dataModel.recordValue.toString()

        binding.startGameButton.setOnClickListener {
            findNavController().navigate(R.id.action_menuFragment_to_gameFragment)
        }
    }

}