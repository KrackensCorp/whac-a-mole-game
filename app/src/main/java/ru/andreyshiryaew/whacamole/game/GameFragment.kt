package ru.andreyshiryaew.whacamole.game

import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import ru.andreyshiryaew.whacamole.R
import ru.andreyshiryaew.whacamole.data.DataModel
import ru.andreyshiryaew.whacamole.databinding.FragmentGameBinding
import java.util.*

class GameFragment : Fragment(R.layout.fragment_game) {

    private lateinit var binding: FragmentGameBinding
    private val dataModel: DataModel by activityViewModels()
    private var gameTimer: CountDownTimer? = null
    private var generalGameScore: Int = 0
    private var moleInHole: Array<Boolean> =
        arrayOf(
            false, false, false,
            false, false, false,
            false, false, false
        )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGameBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startGameTimer(20000)
        startGame()
        for (i in 0..8) {
            getImageView(i).setOnClickListener { checkMole(i) }
        }
    }

    private fun startGameTimer(timeMillis: Long) {
        gameTimer?.cancel()
        gameTimer = object : CountDownTimer(timeMillis, 1000) {
            override fun onTick(timeM: Long) {
                binding.timerTextView.text = ((timeM.toInt() / 1000) + 1).toString()
            }

            override fun onFinish() {
                findNavController().navigate(R.id.action_gameFragment_to_endgameFragment)
                dataModel.score.value = generalGameScore
                dataModel.record.value = generalGameScore
                if (generalGameScore > dataModel.recordValue)
                    dataModel.recordValue = generalGameScore
            }
        }.start()
    }

    private fun checkMole(hole: Int) {
        if (moleInHole[hole]) {
            generalGameScore++
            binding.generalGameScoreTextView.text = generalGameScore.toString()
            hideAllMole()
            startGame()
        }
    }

    private fun startGame() {
        val randomHole = (0..8).random()
        val moleTimer = Timer()

        showMole(randomHole)
        moleTimer.schedule(object : TimerTask() {
            override fun run() {
                if (moleInHole[randomHole]) {
                    hideAllMole()
                    startGame()
                }
            }
        }, 500)
    }

    private fun hideAllMole() {
        moleInHole = arrayOf(
            false, false, false,
            false, false, false,
            false, false, false
        )
        for (i in 0..8) {
            hideMole(i)
        }
    }

    private fun showMole(hole: Int) {
        getImageView(hole).setImageDrawable(activity?.let { it -> ContextCompat.getDrawable(it, R.drawable.mole) })
        moleInHole[hole] = true
    }

    private fun hideMole(hole: Int) {
        getImageView(hole).setImageDrawable(activity?.let { it -> ContextCompat.getDrawable(it, R.drawable.fall) })
        moleInHole[hole] = false
    }

    private fun getImageView(numImageView: Int): ImageView {
        return when (numImageView) {
            0 -> binding.imageView1
            1 -> binding.imageView2
            2 -> binding.imageView3
            3 -> binding.imageView4
            4 -> binding.imageView5
            5 -> binding.imageView6
            6 -> binding.imageView7
            7 -> binding.imageView8
            else -> binding.imageView9
        }
    }
}